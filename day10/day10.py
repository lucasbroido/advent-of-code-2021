import re
arr = open('./day10.txt').read().split('\n')

def containsMatchingBrackets(string):
  bracketPairs = ['\\[\\]', '\\{\\}','\\(\\)','<>']
  return any([True for pair in bracketPairs if re.search(pair,string)])

def stripMatchingBrackets(string):
  bracketPairs = ['\\[\\]', '\\{\\}','\\(\\)','<>']
  for pair in bracketPairs:
    string = re.sub(pair, '',string)
  return string

def getIncorrectCharacterScore(string):
  openingBrackets = ['[', '{', '(','<']
  characterScores = {')': 3, ']': 57, '}': 1197, '>':25137}
  for char in string:
    if char not in openingBrackets:
      return characterScores[char]

def getIncompleteLines(arr):
  incompletes = []
  for line in arr:
    s = line
    while containsMatchingBrackets(s):
      s = stripMatchingBrackets(s)
    score = getIncorrectCharacterScore(s)
    if score is None:
      incompletes.append(s)
  return incompletes

def getMiddleAutoCompleteScore(incompletes):
  closingBracketPoints = {'(': 1,'[': 2,'{': 3,'<':4}
  autoCompleteScores = []
  for line in incompletes:
    seq = reversed(line)
    score = 0
    for char in seq:
      score = 5*score + closingBracketPoints[char]
    autoCompleteScores.append(score)
  return sorted(autoCompleteScores)[len(autoCompleteScores)/2]

def getSyntaxErrorScore(arr):
  count = 0
  for line in arr:
    s = line
    while containsMatchingBrackets(s):
      s = stripMatchingBrackets(s)
    score = getIncorrectCharacterScore(s)
    if score is not None:
      count += score
  return count

print('Part 1:', getSyntaxErrorScore(arr))
print('Part 2:',getMiddleAutoCompleteScore(getIncompleteLines(arr)))



