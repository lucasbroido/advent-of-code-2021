def parseInputByLine(file):
  return open(file).read().split('\n')

def parseInputBySpace(file):
  return open(file).read().split(' ')