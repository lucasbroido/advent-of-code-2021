arr = open('./day09.txt').read().split('\n')
heightMap = []
for line in arr:
  heightMap.append([int(num) for num in line])


def neighbours(arr,x, y):
  neighbours = []
  if x < 0 or x >(len(arr[0])-1) or y < 0 or y > (len(arr)-1):
    return neighbours
  #left
  if x > 0:
    neighbours.append([x-1,y])
  
  #right
  if x < (len(arr[0])-1):
    neighbours.append([x+1,y])

  #up
  if y > 0:
    neighbours.append([x,y-1])
  
  #down
  if y < (len(arr) -1):
    neighbours.append([x,y+1])
  return neighbours

def isLowPoint(arr,x, y, neighbours):
  point =  arr[y][x]
  # print('neighbours are: ', neighbours)
  for n in neighbours:
    if point >= n:
      return False
  return True

def getLowPoints(heightMap):
  lowPoints = []
  for j in range(len(heightMap)):
    for i in range(len(heightMap[0])):
      adjs = [heightMap[x[1]][x[0]] for x in neighbours(heightMap, i, j)]
      if isLowPoint(heightMap, i, j, adjs):
        lowPoints.append([i,j])
  return lowPoints

def getRiskLevel(heightMap, lowPoints):
  riskLevel = 0
  for p in lowPoints:
    riskLevel += heightMap[p[1]][p[0]] + 1
  return riskLevel

def getHigherNeighbours(arr, point):
  pointValue = arr[point[1]][point[0]]
  nextDoors = filter(lambda x: arr[x[1]][x[0]] is not 9 
    and arr[x[1]][x[0]] > pointValue, neighbours(arr, point[0], point[1]))
  return nextDoors

def getBasinCount(point):
  tracker = [point]
  temp = [point]
  while len(temp) > 0:
    for tempPoint in tracker:
      neighs = getHigherNeighbours(heightMap, tempPoint)
      pointsToAdd = [n for n in neighs if n not in tracker]
      temp = pointsToAdd
      tracker += pointsToAdd
  return len(tracker)


lowPoints = getLowPoints(heightMap)     
print('Part 1:',getRiskLevel(heightMap, lowPoints))
basinSizes = [getBasinCount(p) for p in lowPoints]
print('Part 2:',reduce((lambda a, b:a*b), sorted(basinSizes)[-3:]))



  
