arr = list(map(int, open('./day07.txt').read().split(',')))

maxFuel1 = []
maxFuel2 = []
for targetPos in range(max(arr)):
  sumFuel1 = 0
  sumFuel2 = 0
  for crab in arr:
    dist = abs(targetPos-crab)
    sumFuel1 += dist
    sumFuel2 += dist*(dist+1)/2
  maxFuel1.append(sumFuel1)
  maxFuel2.append(sumFuel2)

print('Part 1:',min(maxFuel1))
print('Part 2:',min(maxFuel2))

