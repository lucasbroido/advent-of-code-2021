
arr = open('./day11.txt').read().split('\n')
octoGrid = []
for line in arr:
  octoGrid.append([int(num) for num in line])

# correctStep = [[int(num) for num in line] for line in open('./correctSteps.txt').read().split('\n')

correctSteps =[]
carr = open('./correctSteps.txt').read().split('\n\n')
for grid in carr:
  temp =[]
  for line in grid.split('\n'):
    temp.append([int(num) for num in line])
  correctSteps.append(temp)

def increaseOne(octoGrid):
  for j in range(len(octoGrid)):
    for i in range(len(octoGrid[0])):
      if octoGrid[j][i] is 9:
        # octoGrid = increaseEnergy(octoGrid, [[i,j]])
        octoGrid[j][i] = 0
      else:
        octoGrid[j][i] += 1
  return octoGrid

def neighbours(arr,x, y):
  neighbours = []
  if x > 0:                                     #left
    neighbours.append([x-1,y])   
  if x < (len(arr[0])-1):                       #right
    neighbours.append([x+1,y])   
  if y > 0:                                     #up
    neighbours.append([x,y-1])   
  if y < (len(arr) -1):                         #down
    neighbours.append([x,y+1])   
  if x > 0 and y > 0:                           #top left
    neighbours.append([x-1,y-1])   
  if y > 0 and x < (len(arr[0])-1):             #top right
    neighbours.append([x+1,y-1]) 
  if y < (len(arr) -1) and x > 0:               #bottom left
    neighbours.append([x-1,y+1])   
  if y < (len(arr) -1) and x < (len(arr[0])-1): #bottom right
    neighbours.append([x+1,y+1]) 
  return neighbours

def getFlashingPoints(arr):
  flashingPoints = []
  for j in range(len(octoGrid)):
    for i in range(len(octoGrid[0])):
      if octoGrid[j][i] is 9:
        flashingPoints.append([i, j])
  return flashingPoints

def increaseEnergy(arr, flashingPoints):
  print('flashing points are: ',flashingPoints)
  pointsToFlash = flashingPoints
  temp = []
  while len(pointsToFlash) > 0:
    adjPoints = []
    tempFlash = []
    print('pointsToFlash:', pointsToFlash)
    for p in pointsToFlash:
      # print('neighbours of',p,'are',neighbours(arr, p[0], p[1]))
      adjPoints+= filter(lambda x: x not in adjPoints and x not in temp, neighbours(arr, p[0], p[1]))
      temp += adjPoints
      print('adjPoints: ',sorted(adjPoints))
    
    for point in adjPoints:
      if arr[point[1]][point[0]] is 9 and point not in temp:
        tempFlash.append(point)
        arr[point[1]][point[0]] = 0
      elif point not in temp:
        temp.append(point)
        arr[point[1]][point[0]] += 1

    # for o in tempFlash:
    #   arr[o[1]][o[0]] = 0

    pointsToFlash = tempFlash
    print('tempFlash: ', sorted(tempFlash))
    print
    
  
  return arr
    
def pprint(grid, correctGrid=None):
  for j in range(len(grid)):
    for i in range(len(grid[0])): 
      if correctGrid is not None and grid[j][i] is not correctGrid[j][i]:
        print(' ' + '\x1b[0;30;41m' + str(grid[j][i]) +'\x1b[0m',end=""),
      else:
        print(' '+str(grid[j][i]) ,end="")
    print()

  print()
  print('---------------------')

steps = 2

pprint(octoGrid)
for i in range(steps):
  octoGrid = increaseOne(octoGrid)
  flashP = getFlashingPoints(octoGrid)
  octoGrid = increaseEnergy(octoGrid, flashP)
  print('---------------------')
  print('        Step', i+1)
  print('---------------------')
  pprint(octoGrid, correctSteps[i])



