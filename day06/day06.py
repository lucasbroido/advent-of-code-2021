arr = list(map(int, open('./day06.txt').read().split(',')))

def newFishies(arr, days):
  fishDict = {0:0, 1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0}

  for item in arr:
    fishDict[item] += 1

  for d in range(days):
    dIndex = 0
    babies = fishDict[0]
    while dIndex < 8:
      fishDict[dIndex] = fishDict[dIndex+1]
      dIndex += 1
    fishDict[6] += babies
    fishDict[8] = babies
  return fishDict


print('Part 1: ',sum(newFishies(arr, 80).values()))
print('Part 2: ',sum(newFishies(arr, 256).values()))