import * as fs from "fs";

interface Point {
  x: number;
  y: number;
}

class Line {
  point1: Point;
  point2: Point;

  constructor(point1: Point, point2: Point) {
    this.point1 = point1;
    this.point2 = point2;
  }

  isHorizontalOrVertical = (): boolean =>
  (this.point1.x == this.point2.x || this.point1.y == this.point2.y);

}

const pointsAlongLine = (line: Line) => {
 
  let points: Point[] = [];
  const deltaX = line.point2.x - line.point1.x;
  const deltaY = line.point2.y - line.point1.y;
  const numPoints = Math.max(Math.abs(deltaY), Math.abs(deltaX));

  for (let i=0; i<=numPoints; i++) {
    let x = line.point1.x + (deltaX/numPoints)*i || 0;
    let y = line.point1.y + (deltaY/numPoints)*i || 0;    
    points.push({x:x, y:y});
  }
  return points;
}

let allLines: Line[] = [];
let maxGridX: number = 0;
let maxGridY: number = 0;

const inputLines: string[] = fs.readFileSync("day05.txt").toString().split("\n");

inputLines.forEach(line => {
  let lineString: string[] = line.split(' -> ');
  let startCoords = lineString[0].split(',').map(Number);
  let startPoint: Point = {x: startCoords[0], y: startCoords[1]};
  let endCoords = lineString[1].split(',').map(Number);
  let endPoint: Point = {x: endCoords[0], y: endCoords[1]};
  let lineToAdd = new Line(startPoint, endPoint);
  allLines.push(lineToAdd);

  maxGridX = Math.max(lineToAdd.point1.x, lineToAdd.point2.x, maxGridX);
  maxGridY = Math.max(lineToAdd.point1.y, lineToAdd.point2.y, maxGridY);

});

const calculateIntersections = (coveredPoints: number[][]): number => {
  let intersections = 0;
  for (let j=0; j<=maxGridY; j++) 
    for (let i=0; i<=maxGridX; i++) 
      if (coveredPoints[j][i] > 1) intersections += 1;
  return intersections;
}

const getCoveredPoints = (lines: Line[]) => {
  let coveredPoints = Array(maxGridY + 1)
    .fill(0)
    .map(() => Array(maxGridX + 1).fill(0));
  lines.forEach(line => {
    let pointsToCover = pointsAlongLine(line);
    pointsToCover.forEach(point => coveredPoints[point.y][point.x] += 1);
  });
  return coveredPoints;
}

const horizontalVerticalIntersections = calculateIntersections(
    getCoveredPoints(
        allLines.filter(line => line.isHorizontalOrVertical())
));
const allIntersections = calculateIntersections(getCoveredPoints(allLines));

console.log('Part 1: ', horizontalVerticalIntersections);
console.log('Part 2: ', allIntersections);


