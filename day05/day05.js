"use strict";
exports.__esModule = true;
var fs = require("fs");
var Line = /** @class */ (function () {
    function Line(point1, point2) {
        var _this = this;
        this.isHorizontalOrVertical = function () {
            return (_this.point1.x == _this.point2.x || _this.point1.y == _this.point2.y);
        };
        this.point1 = point1;
        this.point2 = point2;
    }
    return Line;
}());
var pointsAlongLine = function (line) {
    var points = [];
    var deltaX = line.point2.x - line.point1.x;
    var deltaY = line.point2.y - line.point1.y;
    var numPoints = Math.max(Math.abs(deltaY), Math.abs(deltaX));
    for (var i = 0; i <= numPoints; i++) {
        var x = line.point1.x + (deltaX / numPoints) * i || 0;
        var y = line.point1.y + (deltaY / numPoints) * i || 0;
        points.push({ x: x, y: y });
    }
    return points;
};
var allLines = [];
var maxGridX = 0;
var maxGridY = 0;
var inputLines = fs.readFileSync("day05.txt").toString().split("\n");
inputLines.forEach(function (line) {
    var lineString = line.split(' -> ');
    var startCoords = lineString[0].split(',').map(Number);
    var startPoint = { x: startCoords[0], y: startCoords[1] };
    var endCoords = lineString[1].split(',').map(Number);
    var endPoint = { x: endCoords[0], y: endCoords[1] };
    var lineToAdd = new Line(startPoint, endPoint);
    allLines.push(lineToAdd);
    maxGridX = Math.max(lineToAdd.point1.x, lineToAdd.point2.x, maxGridX);
    maxGridY = Math.max(lineToAdd.point1.y, lineToAdd.point2.y, maxGridY);
});
var calculateIntersections = function (coveredPoints) {
    var intersections = 0;
    for (var j = 0; j <= maxGridY; j++)
        for (var i = 0; i <= maxGridX; i++)
            if (coveredPoints[j][i] > 1)
                intersections += 1;
    return intersections;
};
var getCoveredPoints = function (lines) {
    var coveredPoints = Array(maxGridY + 1)
        .fill(0)
        .map(function () { return Array(maxGridX + 1).fill(0); });
    lines.forEach(function (line) {
        var pointsToCover = pointsAlongLine(line);
        pointsToCover.forEach(function (point) { return coveredPoints[point.y][point.x] += 1; });
    });
    return coveredPoints;
};
var horizontalVerticalIntersections = calculateIntersections(getCoveredPoints(allLines.filter(function (line) { return line.isHorizontalOrVertical(); })));
var allIntersections = calculateIntersections(getCoveredPoints(allLines));
console.log('Part 1: ', horizontalVerticalIntersections);
console.log('Part 2: ', allIntersections);
