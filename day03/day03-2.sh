#!/bin/bash

function calc() {
  tempFile=./temp
  inputFile=$1
  index=1

  while [ $(wc -l < $inputFile) -gt 1 ]
  do
    if [ $2 == "oxygen" ]; then
      awk 'BEGIN { FS="" } { sum += $pos; lines[NR] = $0 } END { bit = (sum >= NR/2); for (i=0; i<length(lines); i++) if (substr(lines[i],pos,1) == bit) print lines[i] }' pos="$index" < $inputFile > $tempFile${index}
    else
      awk 'BEGIN { FS="" } { sum += $pos; lines[NR] = $0 } END { bit = (sum < NR/2); for (i=0; i<length(lines); i++) if (substr(lines[i],pos,1) == bit) print lines[i] }' pos="$index" < $inputFile > $tempFile${index}
    fi
    inputFile=$tempFile${index}
    ((index++))
  done
  echo $(cat $tempFile$((index-1)))
  rm temp*
}

oxy=$(calc $1 "oxygen")
scrubber=$(calc $1 "scrubber")

oxy2Dec=$(echo 'ibase=2;obase=A;' $(calc $1 "oxygen")|bc) 
scrub2Dec=$(echo 'ibase=2;obase=A;' $(calc $1 "scrubber")|bc)
result=$(( oxy2Dec * scrub2Dec))
echo $result








