#!/usr/bin/awk -f
BEGIN { FS="" } 
    
{ 
  for (i=1; i<=NF; i++) 
    sum[i] += $i
}

END {
  for (i=1; i<=NF; i++) 
    num += (sum[i] > NR/2)*2^(NF-i); 
  
  print num * ((2^NF-1) - num)
}

