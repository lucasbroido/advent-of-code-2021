#!/usr/bin/awk -f
BEGIN { FS="" } 
    
{ 
  sum += $pos
  lines[NR] = $0
} 
   
END {
  bit = (sum >= NR/2)
  for (i=0; i<length(lines); i++)
    if (substr(lines[i],pos,1) == bit) 
      print lines[i]
}

#!/usr/bin/awk -f
# awk 'BEGIN { FS="" } { sum += $pos; lines[NR] = $0 } END { bit = (sum >= NR/2); for (i=0; i<length(lines); i++) if (substr(lines[i],pos,1) == bit) print lines[i] }'
