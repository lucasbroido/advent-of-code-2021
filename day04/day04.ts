class BingoCard {
  cardRows: number[][] = [];
  markedRows: boolean[][] = [];
  markedNumbers: number[] = [];

  constructor(cardString: string) {
    let rows = cardString.split('\n');
    rows.forEach((row) => {
      let numRow = row.split(/\b\s+\b/g).map(Number);
      this.cardRows.push(numRow);
    });

    this.markedRows = Array(this.cardRows.length)
      .fill(false)
      .map(() => Array(this.cardRows[0].length).fill(false));
  }

  markNumber = (numberToCheck: number): void => {
    this.cardRows.forEach((row, rowIndex) => {
      row.forEach((n, numIndex) => {
        if (n == numberToCheck) {
          this.markedRows[rowIndex][numIndex] = true;
          this.markedNumbers.push(numberToCheck);
        }
      });
    });
  };

  checkRows = (rowsToCheck: boolean[][]): boolean => rowsToCheck.some(row => row.every(slot => slot));

  transposeMatrix = (arr: boolean[][]): boolean[][] => {
    let columns = [];
    for (let i=0; i<arr.length; i++) columns.push(arr.map(x => x[i]));
    return columns;
  }

  isBingo = () => this.checkRows(this.markedRows) || this.checkRows(this.transposeMatrix(this.markedRows));  

  sumUnmarkedNumbers = (): number => {
    let unmarked: number[] = [];
    this.markedNumbers
    this.cardRows.forEach(row => {
      row.forEach(num => {
        if (!this.markedNumbers.includes(num)) unmarked.push(num);
      });
    });
    return unmarked.length > 0 ? unmarked.reduce((a, b) => a + b) : 0;
  }
}

import * as fs from "fs";

const arr = fs.readFileSync("day04.txt").toString().split("\n\n");
const bingoNums: number[] = arr[0].split(",").map(Number);
let bingoCards: BingoCard[] = [];

arr.slice(1).forEach((card) => bingoCards.push(new BingoCard(card)));

const firstWinningBoardScore = (bingoNums: number[], bingoCards: BingoCard[]): number => {
  for (let numIndex in bingoNums) {
    let currNumber = bingoNums[numIndex];
    for (let i=0; i<bingoCards.length; i++) {
      let card = bingoCards[i];
      card.markNumber(currNumber);
      if (card.isBingo()) return (card.sumUnmarkedNumbers() * currNumber);
    } 
  }
  return -1;
}

const everyoneHasBingo = (bingoCards: BingoCard[]): boolean => bingoCards.every(card => card.isBingo());

const lastWinningBoardScore = (bingoNums: number[], bingoCards: BingoCard[]): number => {
  let winningCard!: BingoCard; 
  let winningNumber!: number; 

  for (let numIndex in bingoNums) {
    let currNumber = bingoNums[numIndex];
    for (let i=0; i<bingoCards.length; i++) {
      if (everyoneHasBingo(bingoCards)) return (winningCard.sumUnmarkedNumbers() * winningNumber);
      let card = bingoCards[i];
      card.markNumber(currNumber);
      if (card.isBingo()) { 
        winningCard = card;
        winningNumber = currNumber;
      }
    }
  }
  return -1;
}

console.log('Part 1: ',firstWinningBoardScore(bingoNums, bingoCards));
console.log('Part 2: ',lastWinningBoardScore(bingoNums, bingoCards));

