"use strict";
exports.__esModule = true;
var BingoCard = /** @class */ (function () {
    function BingoCard(cardString) {
        var _this = this;
        this.cardRows = [];
        this.markedRows = [];
        this.markedNumbers = [];
        this.markNumber = function (numberToCheck) {
            _this.cardRows.forEach(function (row, rowIndex) {
                row.forEach(function (n, numIndex) {
                    if (n == numberToCheck) {
                        _this.markedRows[rowIndex][numIndex] = true;
                        _this.markedNumbers.push(numberToCheck);
                    }
                });
            });
        };
        this.checkRows = function (rowsToCheck) { return rowsToCheck.some(function (row) { return row.every(function (slot) { return slot; }); }); };
        this.transposeMatrix = function (arr) {
            var columns = [];
            var _loop_1 = function (i) {
                columns.push(arr.map(function (x) { return x[i]; }));
            };
            for (var i = 0; i < arr.length; i++) {
                _loop_1(i);
            }
            return columns;
        };
        this.isBingo = function () { return _this.checkRows(_this.markedRows) || _this.checkRows(_this.transposeMatrix(_this.markedRows)); };
        this.sumUnmarkedNumbers = function () {
            var unmarked = [];
            _this.markedNumbers;
            _this.cardRows.forEach(function (row) {
                row.forEach(function (num) {
                    if (!_this.markedNumbers.includes(num))
                        unmarked.push(num);
                });
            });
            return unmarked.length > 0 ? unmarked.reduce(function (a, b) { return a + b; }) : 0;
        };
        var rows = cardString.split('\n');
        rows.forEach(function (row) {
            var numRow = row.split(/\b\s+\b/g).map(Number);
            _this.cardRows.push(numRow);
        });
        this.markedRows = Array(this.cardRows.length)
            .fill(false)
            .map(function () { return Array(_this.cardRows[0].length).fill(false); });
    }
    return BingoCard;
}());
var fs = require("fs");
var arr = fs.readFileSync("day04.txt").toString().split("\n\n");
var bingoNums = arr[0].split(",").map(Number);
var bingoCards = [];
arr.slice(1).forEach(function (card) { return bingoCards.push(new BingoCard(card)); });
var firstWinningBoardScore = function (bingoNums, bingoCards) {
    for (var numIndex in bingoNums) {
        var currNumber = bingoNums[numIndex];
        for (var i = 0; i < bingoCards.length; i++) {
            var card = bingoCards[i];
            card.markNumber(currNumber);
            if (card.isBingo())
                return (card.sumUnmarkedNumbers() * currNumber);
        }
    }
    return -1;
};
var everyoneHasBingo = function (bingoCards) { return bingoCards.every(function (card) { return card.isBingo(); }); };
var lastWinningBoardScore = function (bingoNums, bingoCards) {
    var winningCard;
    var winningNumber;
    for (var numIndex in bingoNums) {
        var currNumber = bingoNums[numIndex];
        for (var i = 0; i < bingoCards.length; i++) {
            if (everyoneHasBingo(bingoCards))
                return (winningCard.sumUnmarkedNumbers() * winningNumber);
            var card = bingoCards[i];
            card.markNumber(currNumber);
            if (card.isBingo()) {
                winningCard = card;
                winningNumber = currNumber;
            }
        }
    }
    return -1;
};
console.log('Part 1: ', firstWinningBoardScore(bingoNums, bingoCards));
console.log('Part 2: ', lastWinningBoardScore(bingoNums, bingoCards));
