arr = open('./day08.txt').read().split('\n')

signalPatterns = []
outputValues = []

for line in arr:
    pattern = line.split(' | ')[0]
    digits = line.split(' | ')[1]
    signalPatterns.append([''.join(sorted(signal))
                           for signal in pattern.split(' ')])
    outputValues.append([''.join(sorted(dig)) for dig in digits.split(' ')])

digitLengths = {0: 6, 1: 2, 2: 5, 3: 5, 4: 4, 5: 5, 6: 6, 7: 3, 8: 7, 9: 6}

uniqueLengths = [2, 3, 4, 7]

def getUniqueDigits(stringInputs):
  numberToSymbols = {0: 'None',1: 'None', 2: 'None', 3: 'None', 4: 'None',
                    5: 'None', 6: 'None', 7: 'None', 8: 'None', 9: 'None'}
    # add unique length strings
  for symbol in stringInputs:
      if len(symbol) in uniqueLengths:
          for key, value in digitLengths.items():
              if len(symbol) == value:
                  numberToSymbols[key] = symbol
  return numberToSymbols

def numWithinNumber(smallString, bigString):
  for char in smallString:
    if char not in bigString:
      return False
  return True

def adjustNumberOne(numberToSymbols, symbol):
  displayConfig = {'a': None, 'b': None, 'c': None,
                  'd': None, 'e': None, 'f': None, 'g': None}
  #adjust number 1
  if numberToSymbols[1][0] in symbol:
    displayConfig['f'] = numberToSymbols[1][1]
    displayConfig['c'] = numberToSymbols[1][0]
  else:
    displayConfig['c'] = numberToSymbols[1][1]
    displayConfig['f'] = numberToSymbols[1][0]

  return displayConfig

def matchNumbers(signalPatterns):
  symbolDictList = []
  for values in signalPatterns:
    numberToSymbols = getUniqueDigits(values)

    for symbol in sorted(values, key=len):

        # number 3
        if len(symbol) is 5 and numWithinNumber(numberToSymbols[7], symbol):
            numberToSymbols[3] = symbol

        # number 6
        if len(symbol) is 6 and not numWithinNumber(numberToSymbols[1], symbol):
            numberToSymbols[6] = symbol
            displayConfig = adjustNumberOne(numberToSymbols, symbol)

        # number 9
        if len(symbol) is 6 and numWithinNumber(numberToSymbols[4], symbol):
            numberToSymbols[9] = symbol

    #number 2, 5 & 0
    twoAndFive = filter(lambda x: len(x) is 5 and x not in numberToSymbols[3],values)
    if displayConfig['f'] in twoAndFive[0]:
      numberToSymbols[2] = twoAndFive[0]
      numberToSymbols[5] = twoAndFive[1]
    else:
      numberToSymbols[5] = twoAndFive[0]
      numberToSymbols[2] = twoAndFive[1]
    numberToSymbols[0] = filter(lambda x: len(x) is 6 and x not in numberToSymbols[6] and x not in numberToSymbols[9] ,values)[0]

    symbolDictList.append(numberToSymbols)
  return symbolDictList


symbolDictList = matchNumbers(signalPatterns)

count = 0
index = 0
uniqueDigits = 0
for output in outputValues:
  num = ''
  symbolDict = symbolDictList[index]
  for val in output:
    if len(val) in uniqueLengths:
      uniqueDigits += 1
    for key, value in symbolDict.items():
      if value == val:
        num += str(key)
  index+=1
  count += int(num)
print('Part 1:', uniqueDigits)
print('Part 2:',count)

